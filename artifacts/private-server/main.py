import logging
import os
import signal
import sys
from types import FrameType

import psycopg2
import psycopg2.extras
from flask import Flask
from flask import Response
from flask import jsonify


# configure with environment variables
LISTEN_HOST = os.getenv('LISTEN_HOST', 'localhost')
LISTEN_PORT = int(os.getenv('LISTEN_PORT', 5000))
LOG_LEVEL = os.getenv('LOG_LEVEL', 'info')
DEBUG_MODE = bool(os.getenv('DEBUG_MODE'))
PG_CONNECTION_STRING = os.getenv('PG_CONNECTION_STRING')

_SERVICE_NAME = 'private-server'

# initialize logging
logging.basicConfig(format='[%(asctime)s %(levelname)s] %(message)s')
log_level = getattr(logging, LOG_LEVEL.upper())

# initialize flask app
app = Flask(_SERVICE_NAME)
app.config.update(
    DEBUG=DEBUG_MODE,
    JSONIFY_PRETTYPRINT_REGULAR=DEBUG_MODE,
)
app.logger.setLevel(log_level)
logging.getLogger('werkzeug').setLevel(log_level)

# global cursor initialized below
cursor = None


@app.route('/parrot')
def parrot() -> Response:
    """
    Randomly select a parrot from the database and return it.
    Return JSON, for example:
    {
        "name": "Pizza Parrot",
        "url": "https://cultofthepartyparrot.com/parrots/pizzaparrot.gif"
    }
    """
    global cursor
    cursor.execute('SELECT name, url FROM parrot ORDER BY random() LIMIT 1')
    parrot = cursor.fetchone()

    response = jsonify({
        'name': parrot['name'],
        'url': parrot['url'],
    })
    response.status_code = 200

    return response


def main() -> None:
    if not PG_CONNECTION_STRING:
        app.logger.error('PG_CONNECTION_STRING must be set')
        sys.exit(1)

    global cursor
    conn = psycopg2.connect(PG_CONNECTION_STRING)
    cursor = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)

    app.logger.info(f'Starting {_SERVICE_NAME}...')
    app.run(host=LISTEN_HOST, port=LISTEN_PORT)


def shutdown(signal_number: int, stack_frame: FrameType) -> None:
    app.logger.info(f'Shutting down {_SERVICE_NAME}')
    sys.exit()


if __name__ == '__main__':
    signal.signal(signal.SIGTERM, shutdown)
    signal.signal(signal.SIGINT, shutdown)
    main()
