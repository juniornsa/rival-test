CREATE DATABASE parrot_db;

\connect parrot_db

CREATE TABLE parrot (
	"id" varchar(36) NOT NULL,
	"name" varchar(255) NOT NULL,
	"url" varchar(255) NOT NULL,
	CONSTRAINT parrot_pk PRIMARY KEY (id)
);

INSERT INTO parrot ("id","name","url") VALUES
  ('fbc7ead4-ec63-48e1-bf19-f0d9dcca487f','Pizza Parrot','https://cultofthepartyparrot.com/parrots/pizzaparrot.gif'),
  ('0c2c8319-feb9-4cbc-afa3-35ae764b844c','Deal With It Parrot','https://cultofthepartyparrot.com/parrots/dealwithitparrot.gif'),
  ('3e88e98b-8e10-4f9a-a3b2-1f4ac9ca4945','Fast Parrot','https://cultofthepartyparrot.com/parrots/hd/fastparrot.gif'),
  ('1d785324-a2b8-4766-b20d-dffb45451796','Parrot Dad','https://cultofthepartyparrot.com/parrots/parrotdad.gif'),
  ('b7da6002-d435-494d-8fb4-8ef4ff8b4f0f','Coffee Parrot','https://cultofthepartyparrot.com/parrots/coffeeparrot.gif'),
  ('36318847-f932-427b-9476-5da2c7ae906e','Fiesta Parrot','https://cultofthepartyparrot.com/parrots/fiestaparrot.gif'),
  ('6b93975a-1e87-4fa5-bb43-17959b380457','Moonwalking Parrot','https://cultofthepartyparrot.com/parrots/moonwalkingparrot.gif'),
  ('cc29f52b-e888-4f9b-8c81-db61c5b6e54e','Beer Parrot','https://cultofthepartyparrot.com/parrots/parrotbeer.gif'),
  ('87e52f41-8456-4f49-85f5-f9d07ce371c2','Ship It Parrot','https://cultofthepartyparrot.com/parrots/shipitparrot.gif'),
  ('3a24549e-6568-4552-a064-137556403353','Conga Party Parrot','https://cultofthepartyparrot.com/parrots/hd/congapartyparrot.gif');
