import logging
import os
import signal
import sys
from typing import Tuple
from types import FrameType

import requests
from flask import Flask


# configure with environment variables
LISTEN_HOST = os.getenv('LISTEN_HOST', 'localhost')
LISTEN_PORT = int(os.getenv('LISTEN_PORT', 8080))
LOG_LEVEL = os.getenv('LOG_LEVEL', 'info')
DEBUG_MODE = bool(os.getenv('DEBUG_MODE'))
PRIVATE_SERVER_ADDRESS = os.getenv('PRIVATE_SERVER_ADDRESS')

_SERVICE_NAME = 'public-server'
_ERROR_PARROT = 'https://cultofthepartyparrot.com/parrots/hd/sadparrot.gif'

# initialize logging
logging.basicConfig(format='[%(asctime)s %(levelname)s] %(message)s')
log_level = getattr(logging, LOG_LEVEL.upper())

# initialize flask app
app = Flask(_SERVICE_NAME)
app.config.update(DEBUG=DEBUG_MODE)
app.logger.setLevel(log_level)
logging.getLogger('werkzeug').setLevel(log_level)


def _img_tag(url: str, height: int = 25) -> str:
    """
    Return an <img> tag for the provided URL. Uses default height of 25px.
    """
    return f'<img src="{url}" height="{height}"/>'


@app.route('/')
def parrot() -> Tuple[str, int]:
    """
    Retrieve parrot of the day from private-server.
    Return 200 on success, 500 on error.
    """
    status_code = 200
    try:
        response = requests.get(f'{PRIVATE_SERVER_ADDRESS}/parrot').json()
        message = 'The parrot of the day is ' + \
                  f'{response["name"]} {_img_tag(response["url"])}'
    except Exception as e:
        app.logger.error(f'Error retrieving parrot of the day: {e}')
        status_code = 500
        message = f'Error {_img_tag(_ERROR_PARROT)}'

    return message, status_code


def main() -> None:
    if not PRIVATE_SERVER_ADDRESS:
        app.logger.error('PRIVATE_SERVER_ADDRESS must be set')
        sys.exit(1)

    app.logger.info(f'Starting {_SERVICE_NAME}...')
    app.run(host=LISTEN_HOST, port=LISTEN_PORT)


def shutdown(signal_number: int, stack_frame: FrameType) -> None:
    app.logger.info(f'Shutting down {_SERVICE_NAME}')
    sys.exit()


if __name__ == '__main__':
    signal.signal(signal.SIGTERM, shutdown)
    signal.signal(signal.SIGINT, shutdown)
    main()
