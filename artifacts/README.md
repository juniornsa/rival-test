# DevOps challenge artifacts

## Artifacts

### public-server

- `Dockerfile` - used to build the public-server Docker image
- `main.py` - simple flask app which calls out to private-server
- `requirements.txt` - requirements required to run public-server

### private-server

- `Dockerfile` - used to build the private-server Docker image
- `main.py` - simple flask app which responds to public-server's requests, using
the data in postgres
- `parrot.sql` - SQL file which can be used to initialize an instance of
Postgres with the data private-server expects to exist
- `requirements.txt` - requirements required to run private-server

## Running artifacts locally

A `docker-compose.yml` is provided to make it easy to run these artifacts
locally. A `Makefile` is also provided to further simplify building the docker
images, initializing the database, and running the two servers.

Full list of make targets:
- `make` - clean up, build, run
- `make build` - build both servers
- `make build-private` - build private-server Docker image
- `make build-public` - build public-server Docker image
- `make clean` - clean up Docker containers (`docker-compose down`)
- `make run` - initialize postgres and run the servers

After running `make`, public-server will be accessible at http://localhost:9999.

## Publishing to Docker Hub

To make artifacts available to candidates during a challenge, the Docker images
are hosted on Docker Hub under the account
[rivalchallenges](https://hub.docker.com/u/rivalchallenges).

To publish new versions of an artifact:
```
docker login --username=rivalchallenges
docker tag public-server rivalchallenges/public-server
docker push rivalchallenges/public-server
```
